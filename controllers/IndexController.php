<?php

    class IndexController extends MainController{
            
        protected function checkPermisions(){
            return true;
        }
        
        protected function doAction(){
            
            //Llamando a esto vamos a la vista que vendra de la URL
            parent::goView();
            
        }

    }