<footer class="footer">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-3">
          <span class="copyright">Copyright &copy; Qué Bolá Recargas</span>
        </div>
        <div class="col-md-3">
          <ul class="list-inline social-buttons">
            <li class="list-inline-item">
              <a href="#">
                <i class="fab fa-twitter" style="margin-top:15px;"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <i class="fab fa-facebook-f" style="margin-top:15px;"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <i class="fab fa-linkedin-in" style="margin-top:15px;"></i>
              </a>
            </li>
          </ul>
        </div>
        <div class="col-md-3">
          <ul class="list-inline quicklinks">
            <li class="list-inline-item">
                <a><img src="./assets/googleplay.png" width="150px;" /></a>
            </li>
          </ul>
        </div>
        <div class="col-md-3">
          <ul class="list-inline quicklinks">
            <li class="list-inline-item">
              <a href="#">Privacy Policy</a>
            </li>
            <li class="list-inline-item">
              <a href="#">Terms of Use</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </footer>