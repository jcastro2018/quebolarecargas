<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top"><img src="/assets/logo.png" width=75px height=75px /></a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav text-uppercase">
        <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo Util::createURL('Index') ?>">
            <i class="fas fa-home" style="color:red;"></i>
            INICIO
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo Util::createURL('Cubacel') ?>">
                <i class="fas fa-mobile-alt"  style="color:red;"></i>
                CUBACEL
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo Util::createURL('Nauta') ?>">
                <i class="fas fa-wifi" style="color:red;"></i>
                NAUTA
            </a>
          </li>
        </ul>
        <div class="ml-auto">
        <button class="navbar-toggler-right sign-in" type="button">
        <i class="fas fa-sign-in-alt"></i>
        REGÍSTRATE
        </button>
        <button class="navbar-toggler-right log-in" type="button">
        <i class="fas fa-user-alt"></i>
        ENTRAR
        </button>
        </div>
      </div>
    </div>
  </nav>