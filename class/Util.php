<?php

class Util{
    public static function getURL(){
        return $_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST'];
    }

    public static function getURI(){
        return $_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    }

    public static function createURL($name){
        return Util::getURL()."?controller=".$name."Controller&view=".$name;
    }

}