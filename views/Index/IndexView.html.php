<section class="page-section" style="text-align:center; height:429px;">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <?php 
            $i = 0;
            foreach(array_diff(scandir('./assets/banners/'), array(".", "..")) as $value){
                $i++;
                if($i==1){
                    echo '<div class="carousel-item active">
                    <img class="d-block w-100" src="/assets/banners/'.$value.'" height="420px" alt="Promotion">
                    </div>';
                }else{
                    echo '<div class="carousel-item">
                    <img class="d-block w-100" src="/assets/banners/'.$value.'" height="420px" alt="Promotion">
                    </div>';
                }
            }
            ?>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>
<!-- Services -->
<section class="page-section" id="services" style="margin-top:50px;">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Ventajas</h2>
        </div>
      </div>
        <div class="">
                <div class="card-deck card-deck_2 w100p recharge_four_part_top">
                <div class="col-md-3">
                    <div class=" text-center">
                    <div class="card-body">
                        <img alt="..." class="rounded" src="/assets/icons/New Project(8).png">
                        <h3 class="card-title">
                        <b>Bajo costo</b>
                        </h3>
                    </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="text-center">
                    <div class="card-body">
                        <img alt="..." class="rounded" src="/assets/icons/New Project (7).png">
                        <h3 class="card-title">
                        <b>súper rápido</b>
                        </h3>
                    </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="text-center">
                    <div class="card-body">
                        <img alt="..." class="rounded" src="/assets/icons/sencilo.png">
                        <h3 class="card-title">
                        <b>Sencillo</b>
                        </h3>
                    </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="text-center">
                    <div class="card-body">
                        <img alt="..." class="rounded" src="/assets/icons/De_confianza.png">
                        <h3 class="card-title">
                        <b>De confianza</b>
                        </h3>
                    </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
  </section>