<?php
abstract class MainController
{
    //No tocar: Es general primero se revisan los permisos y luego se hace lo que toque
    function __construct(){
        if($this->checkPermisions()==true){
            $this->doAction();
        }
        else{
            header("Location: ".Util::getURL()."/index.php?controller=IndexController&view=Index");
        }    
    }

    //En este metodo realizaremos siempre las comprobaciones generales
    abstract protected function checkPermisions();

    //En este metodo realizaremso las llamadas y funciones
    abstract protected function doAction();

    //Aqui haremos el include necesario para recoger
    protected function goView(){
        echo '<html><head>
        
        <meta charset="utf-8">
        
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        
        <title>Qué Bolá Recargas</title>
        
        <!--Styles CSS-->
        <link href="/includes/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="/includes/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css">
        <link href="/includes/css/agency.css" rel="stylesheet">

        <!-- Bootstrap core JavaScript -->
        <script src="/includes/vendor/jquery/jquery.min.js"></script>
        <script src="/includes/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        ';

            //Aqui cargamos el css de la vista
            if (file_exists('./views/'.$_GET['view']."/".$_GET['view'].'View.css')){
                echo '<link rel="stylesheet" type="text/css" href="'.'/views/'.$_GET['view']."/".$_GET['view'].'View.css'.'">';
                echo '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>';
            }

            //Aqui cargamos el js de la pagina
            if(file_exists('./views/'.$_GET['view']."/".$_GET['view'].'View.js')){
                echo '<script src="'.'/views/'.$_GET['view']."/".$_GET['view'].'View.js'.'"></script>';
            }

        //Aqui cargamos la vista general
        echo "<head><body>";
        
        include './includes/header.html.php';
        include './views/'.$_GET['view']."/".$_GET['view'].'View.html.php';
        include './includes/footer.html.php';
        
        echo "<body></html>";

    }




}