<?php
    //Descomentar para ver los errores
    error_reporting(E_ALL);
    ini_set('display_errors', '1');

    //Cargamos todas las utilidades
    foreach(array_diff(scandir('./class/'), array(".", "..")) as $key){
        include './class/'.$key;
    }
    //Fichero general por el que pasara todo
    if(isset($_GET['controller'])){
        //Con esto cargamos solo el controlar y el objeto que solicitamos
        include './controllers/MainController.php';
        include './controllers/'.$_GET['controller'].'.php';
        new $_GET['controller']();
    
    }else{
        header("Location: ".Util::getURL()."/index.php?controller=IndexController&view=Index");
    }